import java.util.Random;
import java.util.Scanner;

public class ZadaniaNr5 {

   private static Scanner scanner = new Scanner(System.in);
   private static char input ;
   private static int ASCII_Code;

    public static int return_ASCII_Code()
    {
        input = scanner.next().charAt(0); // internet solution.
        ASCII_Code = (int) input;
        return ASCII_Code;
    }

    public static char codeToASCII(int code)
    {
        char c = (char) code;
        return c;
    }


    public static void drawSquereWithAsciiCodes(int size)
    {
        Random random = new Random();
        int randomNumber ;
        for (int i = 0; i < size ; i++) {
            for (int j = 0; j < size; j++) {
                randomNumber = random.nextInt(254);
                System.out.print(" "+(char)randomNumber);
            }
            System.out.println();
        }
    }

    public static void drawTriangleWithAsciiCodes(int width,int height)
    {
        int halfOfWidth = width /2;
        for (int i = 0; i < height ; i++) {
            for (int j = 0; j < width ; j++)
            {
            if(j >= (halfOfWidth +i) || j <= (halfOfWidth -i))
                {
                    System.out.print("  ");
                }
            else
                {
                    System.out.print(" A");
                }
            }
            System.out.println();
        }
    }

}
