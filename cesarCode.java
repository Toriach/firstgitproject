import java.util.Scanner;

public class cesarCode {
    Scanner scanner = new Scanner(System.in);
    private static String input,output = "";
    private static char c;
    private static int code;

    public static String C_Coder(String input,int offset)
    {
        for (int i = 0; i < input.length() ; i++)
        {
            c = input.charAt(i);
            code = (int) c;
            code += offset;
            output = output + (char) code;
        }
        return output;
    }

    public static String deszyfrer(String input,int offset)
    {
        for (int i = 0; i < input.length() ; i++)
        {
            c = input.charAt(i);
            code = (int) c;
            code -= offset;
            output = output + (char) code;
        }
        return output;
    }
}
