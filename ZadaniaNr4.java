import java.util.Scanner;

public class ZadaniaNr4 {

    static String reversedString ="";

    public static void reverseNumbers(String input)
    {
        for (int i = 0; i < input.length() ; i++) {
            if (((input.length()-1) -i) >= 0){
                reversedString = input.substring(((input.length()-1)-i),input.length());
            }
        }
        System.out.println(reversedString);
    }
// TODO
    public static int reverseNumber(int number){
        int reverse = 0;
        while(number != 0){
            reverse = reverse * 10;
            reverse = reverse + number%10;
            number = number / 10;
        }
        return reverse;
    }


}
