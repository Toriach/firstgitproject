import java.util.Scanner;

public class Rozgrzewka {

        private static Scanner scanner;
        Rozgrzewka()
        {
            scanner = new Scanner(System.in);

            from1to100();
        }
        public static void from1to100()
        {
            for (int i = 0; i < 100; i++) {
                System.out.println(i+1);
            }
        }
        public static void fromXtoY()
        {
            scanner = new Scanner(System.in);
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            for (int i = x; i <= y; i++) {
                System.out.println(i);
            }
        }
        public static void fromXtoYwitchSteps()
        {
            scanner = new Scanner(System.in);
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            int step = scanner.nextInt();
            for (int i = x; i <= y; i+=step) {
                System.out.println(i);
            }
        }
        public static void czyParzyste()
        {
            scanner = new Scanner(System.in);
            int a = scanner.nextInt();
            if(a % 2 == 0){
                System.out.println("liczba jest parzysta");
            }
            else
            {
                System.out.println("liczba jest nie parzysta");
            }
        }


}
