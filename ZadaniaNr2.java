import java.util.Arrays;

public class ZadaniaNr2
{

    public static void findLowestAndHighest(int[] tab)
    {
        int min = tab[0];
        int max = tab[0];
        for (int i = 0; i < tab.length ; i++) {
            if (tab[i] < min) { min = tab[i]; }
            if (tab[i] > max) { max = tab[i]; }
        }
        System.out.println("najmniejsza liczba to: "+min+" Najwieksza liczba to: "+max);
    }

    public static void reverseArray(int[] tab)
    {
        System.out.println("before: ");
        System.out.println(Arrays.toString(tab));
        int tempMin;
        int tempMax;
        for (int i = 0; i < tab.length/2 ; i++)
        {
            tempMin = tab[i];
            tempMax = tab[(tab.length - 1) - i];
            tab[i] = tempMax;
            tab[(tab.length - 1) - i] = tempMin;
        }
        System.out.println("after: ");
        System.out.println(Arrays.toString(tab));
    }


    public static void mostOccurringNumber(int[] tab) {
        int HighestNumOfOccur = 0;
        int mostOccurNumb = 0;

        for (int i = 0; i < tab.length; i++) {
            int NumOfOccur = 0;
            for (int j = 0; j < tab.length ; j++) {
                if(tab[i] == tab[j]) {
                    NumOfOccur++;
                    if(NumOfOccur > HighestNumOfOccur){
                        HighestNumOfOccur++;
                        mostOccurNumb = tab[i];
                    }
                }
            }
        }
        System.out.println("Liczba: "+mostOccurNumb +" pojawiła się: "+HighestNumOfOccur+" razy.");
    }

    public static void mergeArrays(int[] tab1,int[] tab2){
        int mergedTabsLenght = tab1.length + tab2.length;
        int[] finallArray = new int[mergedTabsLenght];
        for (int i = 0; i < mergedTabsLenght; i++) {
            if (i <= tab1.length -1){
                finallArray[i] = tab1[i];
            }
            else{
                finallArray[i] = tab2[i - (tab1.length)];
            }
        }
        System.out.println("finnal array: "+Arrays.toString(finallArray));
    }

    public static void sortedMergeArrays(int[] tab1,int[] tab2){
        int mergedTabsLenght = tab1.length + tab2.length;
        int[] mergedArray = new int[mergedTabsLenght];
        for (int i = 0; i < mergedTabsLenght; i++) {
            if (i <= tab1.length -1){
                mergedArray[i] = tab1[i];
            }
            else{
                mergedArray[i] = tab2[i - (tab1.length)];
            }
        }
       // Arrays.sort(mergedArray); // too easy :D
        System.out.println("finnal array: "+Arrays.toString(mergedArray));
        int temp;
        for (int i = 0; i < mergedArray.length -1 ; i++) {
            for (int j = 0; j < mergedArray.length-1 ; j++) {
                if(mergedArray[j] > mergedArray[j + 1]){
                    temp = mergedArray[j];
                    mergedArray[j] = mergedArray[j + 1];
                    mergedArray[j + 1] = temp;
                }
            }
        }
        System.out.println("finnal sorted array: "+Arrays.toString(mergedArray));
    }













}
