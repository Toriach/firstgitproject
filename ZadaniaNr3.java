import java.util.Arrays;

public class ZadaniaNr3 {

    public static void FibonacciNumbers(int howMany){
        int a1 = 1;
        int a2 = 0;
        int sum;
        for (int i = 0; i <= howMany ; i++) {

            if(i == 0){
                System.out.print(0+" ");
            }
            if(i < 2){
                System.out.print(1+" ");
            }
            else{
                sum = a2 + a1;
                System.out.print(sum + " ");
                a2 = a1;
                a1 = sum;
            }
        }
    }

    public static void splitToArray(int number)
    {
        String splitString = ""+number;
        int[] tempArray = new int[splitString.length()];
        for (int i = 0; i < splitString.length() ; i++) {
            String sumstring = splitString.substring(i,i+1);
            tempArray[i] = Integer.valueOf(sumstring);
            System.out.println(i+" "+tempArray[i]);
        }
        System.out.println(Arrays.toString(tempArray));
    }




}
